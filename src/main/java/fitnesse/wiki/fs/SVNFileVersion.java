package fitnesse.wiki.fs;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public class SVNFileVersion implements FileVersion {
	
	private File file;
	private byte[] content;
	private String author;
	private Date lastModificationTime;

    SVNFileVersion(File file, byte[] content, String author, Date lastModificationTime)
	{
		this.file = file;
		this.content = content;
		this.author = author;
		this.lastModificationTime = lastModificationTime;
	}
	
	public File getFile() {
		return file;
	}

	public InputStream getContent() throws IOException {
		return new ByteArrayInputStream(content);
	}

	public String getAuthor() {
		return author;
	}

	public Date getLastModificationTime() {
		return lastModificationTime;
	}
}
