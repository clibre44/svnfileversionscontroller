package fitnesse.wiki.fs;

/**
 * Provides Subversion integration with FitNesse using the CM_SYSTEM mechanism available in FitNesse version 20100303
 */

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.tmatesoft.svn.core.ISVNDirEntryHandler;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCommitClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import fitnesse.wiki.PageData;
import fitnesse.wiki.RecentChanges;
import fitnesse.wiki.VersionInfo;
import fitnesse.wiki.WikiPage;
import fitnesse.wiki.WikiPageProperties;

public class SVNFileVersionsController implements VersionsController, RecentChanges {

    private static Map<String, SVNClientManager> clientManagerCache = new HashMap<String, SVNClientManager>();
	private static String hostname;
	private static String workingDir;
	private SVNClientManager clientManager;
	private VersionsController simpleVC = new SimpleFileVersionsController();
    private static final Logger LOGGER = Logger.getLogger(SVNFileVersionsController.class.getName());
	private int historyEntries = 200;
	
	private static final String HISTORY_ENTRIES = "history_entries";
	
	static {
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			hostname = "(unknown host)";
		}

		try {
			workingDir = new File(".").getCanonicalPath();
		} catch (IOException e) {
			workingDir = "(unknown directory)";
		}

		// SVN setup
		DAVRepositoryFactory.setup();
		SVNRepositoryFactoryImpl.setup();
		FSRepositoryFactory.setup();
	}

	public SVNFileVersionsController(Properties properties) throws IOException {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "constructor");
        Level logLevel;
        try {
            logLevel = Level.parse((String) properties.get("LOG_LEVEL"));
        } catch(IllegalArgumentException ex) {
            logLevel = Level.OFF;
        }
        LOGGER.setLevel(logLevel);

        String credentials = (String) properties.get("SVN_CREDENTIALS");

		try {
			this.historyEntries = Integer.parseInt((String)properties.get(HISTORY_ENTRIES));
		} catch (NumberFormatException nfEx) {
            LOGGER.warning("History entry number is wrong. Using default: " + this.historyEntries);
		}
		this.clientManager = getClientManager(credentials);
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "constructor");
	}

	private void svnCommit(FileVersion... files) {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "svnCommit");

        SVNCommitClient cc = clientManager.getCommitClient();
		Set<File> fileVersions = new HashSet<File>();
		for (FileVersion fv : files) {
			fileVersions.add(fv.getFile());
			fileVersions.add(fv.getFile().getParentFile());
		}

        if(cc == null) {
            LOGGER.severe("CommitClient is null.");
            throw new IllegalStateException("CommitClient is null.");
        }

		if (!fileVersions.isEmpty())
		{
			try {
				cc.doCommit(fileVersions.toArray(new File[0]), false,
						"FitNesse checkin from " + hostname + ":" + workingDir,
						null, null, false, false, SVNDepth.INFINITY);
			} catch (SVNException svnEx) {
                LOGGER.severe("Commit failed.");
				throw new IllegalStateException("Commit failed.", svnEx);
			}
		}
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "svnCommit");
	}

	private SVNClientManager getClientManager(
			String credentials) throws IOException {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "getClientManager");
		SVNClientManager mgr = clientManagerCache.get(credentials);
		if (mgr != null) {
			return mgr;
		}

        String credArr[] = credentials.split(":");
        String user;
        String password;
        if(credArr != null && credArr.length == 2) {
            user = credArr[0];
            password = credArr[1];
        } else {
            LOGGER.severe("Credentials were not given in the right format (user:password)");
            throw new IllegalStateException("Credentials were not given in the right format (user:password)");
        }
		mgr = SVNClientManager.newInstance(
				SVNWCUtil.createDefaultOptions(false), user, password);
		clientManagerCache.put(credentials, mgr);
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "getClientManager", mgr);
		return mgr;
	}

	public FileVersion[] getRevisionData(String revision, File... files) {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "getRevisionData");

        if(revision == null)
		{
			return simpleVC.getRevisionData(null, files);
		}
		
		List<FileVersion> result = new ArrayList<FileVersion>();
		
		try {
			for (File f : files)
			{
				SVNStatus svnStatus = clientManager.getStatusClient().doStatus(f, false);

				if (svnStatus == null)
				{
					continue;
				}

				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				clientManager.getWCClient().doGetFileContents(f, SVNRevision.parse(revision), SVNRevision.parse(revision), false, bos);
				
				FileVersion fv = new SVNFileVersion(f, bos.toByteArray(), svnStatus.getAuthor(), svnStatus.getCommittedDate());
				result.add(fv);
			}
		} catch (SVNException svnEx) {
            LOGGER.severe(svnEx.getMessage());
            throw new IllegalStateException(svnEx);
		}
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "getRevisionData", result);
		return result.toArray(new FileVersion[0]);
	}

	public Collection<? extends VersionInfo> history(File... files) {
		final List<SVNVersionInfo> result = new ArrayList<SVNVersionInfo>();
        LOGGER.entering(SVNFileVersionsController.class.getName(), "history");
		try {
			clientManager.getLogClient().doList(
					files[0], 
					SVNRevision.UNDEFINED, 
					SVNRevision.HEAD, 
					false, 
					SVNDepth.INFINITY, 
					this.historyEntries,
					new ISVNDirEntryHandler() {
						
						public void handleDirEntry(SVNDirEntry dirEntry) throws SVNException {
							result.add(new SVNVersionInfo(dirEntry.getName(), dirEntry.getAuthor(), dirEntry.getDate(), dirEntry.getCommitMessage()));
							
						}
					}
					);
		} catch (SVNException svnEx) {
            LOGGER.severe(svnEx.getMessage());
			throw new IllegalStateException(svnEx);
		}
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "history", result);
		return result;
	}

	public VersionInfo makeVersion(FileVersion... files) throws IOException {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "makeVersion");
		simpleVC.makeVersion(files);
		
		for (FileVersion fv : files) {
			try {
				clientManager.getWCClient().doAdd(fv.getFile().getParentFile(), true, true,
						true, SVNDepth.INFINITY, true, true);
			} catch (SVNException svnEx) {
				throw new IllegalStateException("SVN add failed for file " + fv.getFile().getAbsolutePath(), svnEx);
			}
		}
		
		svnCommit(files);
        VersionInfo result = VersionInfo.makeVersionInfo(files[0].getAuthor(),
            files[0].getLastModificationTime());
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "makeVersion", result);
        return result;
	}

	public VersionInfo addDirectory(FileVersion file) throws IOException {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "addDirectory");
        svnCommit(file);
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "addDirectory");
        //WTF??
		return null;
	}

	public void rename(FileVersion file, File originalFile) throws IOException {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "rename");
		svnCommit(file);
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "addDirectory");
	}

	public void delete(FileVersion... files) {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "delete");
		svnCommit(files);
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "delete");
	}

	public void updateRecentChanges(WikiPage page) {
		// TODO Nothing TODO
	}

	public WikiPage toWikiPage(WikiPage root) {
        LOGGER.entering(SVNFileVersionsController.class.getName(), "toWikiPage");
		FileSystemPage fsPage = (FileSystemPage) root;
	    WikiPage recentChangesPage = createInMemoryRecentChangesPage(fsPage);
	    PageData pageData = recentChangesPage.getData();

	    // No properties, no features.
	    pageData.setProperties(new WikiPageProperties());
	    recentChangesPage.commit(pageData);
        LOGGER.exiting(SVNFileVersionsController.class.getName(), "toWikiPage", recentChangesPage);
	    return recentChangesPage;
	}
	
	private WikiPage createInMemoryRecentChangesPage(FileSystemPage parent) {
	    LOGGER.entering(SVNFileVersionsController.class.getName(), "createInMemoryRecentChangesPage");
	    MemoryFileSystem fileSystem = new MemoryFileSystem();
        WikiPage result = new FileSystemPage(new File(parent.getFileSystemPath(), RECENT_CHANGES),
                RECENT_CHANGES, parent, new MemoryVersionsController(fileSystem));
	    LOGGER.exiting(SVNFileVersionsController.class.getName(), "createInMemoryRecentChangesPage", result);
	    return result;
	  }

}