package fitnesse.wiki.fs;

import java.util.Date;

import fitnesse.wiki.VersionInfo;

public class SVNVersionInfo extends VersionInfo {

    private static final long serialVersionUID = 1L;
	private final String comment;

    SVNVersionInfo(String name, String author, Date creationTime, String comment) {
      super(name, author, creationTime);
      this.comment = comment;
    }

    public String getComment() {
      return comment;
    }
  }
